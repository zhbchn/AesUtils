package com.zhb.smartlocks;

import java.nio.ByteBuffer;
import java.util.Locale;

/**
 * Byte工具
 */

public class ByteUtils {

    private static final String HEADER = "5354";
    private static final String END = "0D0A";

    private static final Integer SEND_HEAD_1=83;
    private static final Integer SEND_HEAD_2=84;
    private static final Integer PASSWORD=20190905;

    private static final Integer SEND_END_1 =10;
    private static final Integer SEND_END_2 =13;

    /**
     * byte转成成整型
     * @param  b byte
     * @return int
     */
    public static int byteToInt(byte b) {
        return (b) & 0xff;
    }

    /**
     * 十六进制字符串转byte[]
     *
     * @param hex 十六进制字符串
     * @return byte[]
     */
    public static byte[] hexStr2Byte(String hex) {
        if (hex == null) {
            return new byte[]{};
        }

        // 奇数位补0
        if (hex.length() % 2 != 0) {
            hex = "0" + hex;
        }

        int length = hex.length();
        ByteBuffer buffer = ByteBuffer.allocate(length / 2);
        for (int i = 0; i < length; i++) {
            String hexStr = hex.charAt(i) + "";
            i++;
            hexStr += hex.charAt(i);
            byte b = (byte) Integer.parseInt(hexStr, 16);
            buffer.put(b);
        }
        return buffer.array();
    }

    private static final char HexCharArr[] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

    private static final String HexStr = "0123456789abcdef";

    public static String byteArrToHex(byte[] btArr) {
        char strArr[] = new char[btArr.length * 2];
        int i = 0;
        for (byte bt : btArr) {
            strArr[i++] = HexCharArr[bt>>>4 & 0xf];
            strArr[i++] = HexCharArr[bt & 0xf];
        }
        return new String(strArr);
    }

    public static byte[] hexToByteArr(String hexStr) {
        char[] charArr = hexStr.toCharArray();
        byte btArr[] = new byte[charArr.length / 2];
        int index = 0;
        for (int i = 0; i < charArr.length; i++) {
            int highBit = HexStr.indexOf(charArr[i]);
            int lowBit = HexStr.indexOf(charArr[++i]);
            btArr[index] = (byte) (highBit << 4 | lowBit);
            index++;
        }
        return btArr;
    }

    /**
     * byte[]转十六进制字符串
     *
     * @return 十六进制字符串
     */
    public static String byteArrayToHexString(byte[] array) {
        if (array == null) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < array.length; i++) {
            buffer.append(byteToHex(array[i]));
        }
        return buffer.toString();
    }

    /**
     * byte转十六进制字符
     *
     * @param b byte
     * @return 十六进制字符
     */
    public static String byteToHex(byte b) {
        String hex = Integer.toHexString(b & 0xFF);
        if (hex.length() == 1) {
            hex = '0' + hex;
        }
        return hex.toUpperCase(Locale.getDefault());
    }



    /**
     * 加密
     * @param controlStr
     * @param str
     * @return
     */
    public static String encryp(String controlStr, String str){
        String encrpStr = HEADER+controlStr+str+END;
        return encrpStr;
    }

    /**
     * 解密
     * @param controlStr
     * @param passwordData
     * @return
     */
    public static String decryp(String controlStr, String passwordData){
        String header = HEADER+controlStr;
        Integer length = passwordData.length();
        String hexStr = passwordData.substring(header.length(),length-END.length());
        return Integer.valueOf(hexStr,16).toString();
    }
    public static String getControlStr(String data){
       return data.substring(HEADER.length(),HEADER.length()+2);
    }

    public static String getValue(String str){
        char [] strArray = str.toCharArray();
        StringBuffer newStr = new StringBuffer();
        for(int i=14 ;i<18;i++){
            newStr.append(strArray[i]);
        }
        return newStr.toString();
    }

    public static String getModelValue(String str){
        char [] strArray = str.toCharArray();
        StringBuffer newStr = new StringBuffer();
        for(int i=18 ;i<20;i++){
            newStr.append(strArray[i]);
        }
        return newStr.toString();
    }

    public static String getTimeValue(String str){
        char [] strArray = str.toCharArray();
        StringBuffer newStr = new StringBuffer();
        for(int i=20 ;i<22;i++){
            newStr.append(strArray[i]);
        }
        return newStr.toString();
    }

    public static byte[] packageSendData(Integer control, Integer data1, Integer data2, Integer data3, Integer data4){
        try {
            byte[] headerByte = hexStr2Byte(HEADER);
            byte[] endByte = hexStr2Byte(END);
            byte[] controlByte = hexStr2Byte(Integer.toHexString(control));
            byte[] passwordByte = hexStr2Byte(Integer.toHexString(PASSWORD));
            byte[] dataByte1 = hexStr2Byte(Integer.toHexString(data1));
            byte[] dataByte2 = hexStr2Byte(Integer.toHexString(data2));
            byte[] dataByte3=  hexStr2Byte(Integer.toHexString(data3));
            byte[] dataByte4= hexStr2Byte(Integer.toHexString(data4));
            return byteMergerAll(headerByte, controlByte, passwordByte, dataByte1,dataByte2,dataByte3,dataByte4,endByte);
        }catch (Exception e){
            return null;
        }

    }
    private static byte[] byteMergerAll(byte[]... values) {
        int length_byte = 0;
        for (int i = 0; i < values.length; i++) {
            length_byte += values[i].length;
        }
        byte[] all_byte = new byte[length_byte];
        int countLength = 0;
        for (int i = 0; i < values.length; i++) {
            byte[] b = values[i];
            System.arraycopy(b, 0, all_byte, countLength, b.length);
            countLength += b.length;
        }
        return all_byte;
    }
}

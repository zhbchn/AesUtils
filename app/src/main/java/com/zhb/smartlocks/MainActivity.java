package com.zhb.smartlocks;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    //明文
    protected String source = "12345678R0000000";
    //密文
    protected String encrypted = "";
    protected String encrypted1 = "";
    //密钥
    protected String key = "1234567812345678";

    private static final String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG,"测试");
        byte [] encryptedMsg=null;
        //加密明文--> 密文66b8a26dc8b1de49ac04b23c1e333f50d96aa42b59151a9e9b5925fc9d95adaf
        try {
            encryptedMsg=AesUtils.encrypt(source, key);
            encrypted = AesUtils.encryptBase64(source, key);
            Log.i(TAG,"密文"+encrypted);
            encrypted1 = AesUtils.encryptToHex(source, key);
            Log.i(TAG,"密文Hex: "+encrypted1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        source = "";
        //解密密文--> 明文
        source = AesUtils.decryptFromBase64(encrypted,key);
        Log.i(TAG,"明文"+source);

        source = AesUtils.decryptFromHex("E10CEF9E567A84907FDBF88CDADD5200",key);
        Log.i(TAG,"明文Hex:"+source);

        source = AesUtils.decrypt(encryptedMsg,key);
        Log.i(TAG,"明文byte:"+source);
    }
}

# AesUtils

#### 介绍
Android中使用AES AES/ECB/NoPadding加密的算法演示，调用Android的Cipher实现。代码参考网上的一些代码，在这里表示感谢。

#### 软件架构
在MainActivity中调用加密算法进行加密，实现了加密后输出byte数组、十六进制字符串、Base64编码，并可以对byte数组、十六进制字符串和Base64编码字符串进行解密。


